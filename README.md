## Task List

A task manager app that allows them to organise their daily tasks.

### Install

get [CocoaPods](http://cocoapods.org)

```shell
$ git clone --depth=1 https://bitbucket.org/Kamiel/tasklist.git
$ cd tasklist
$ pod install
```

### Usage

```shell
$ open tasklist.xcworkspace
```

Enjoy!
